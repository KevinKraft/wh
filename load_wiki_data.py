"""
Load Wikipedia pages into chromadb
"""

import wikipediaapi
import logging
import src.chromadb_helpers
import pandas as pd
from  src.chunker import TextChunker

logging.basicConfig()
logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)


N_CITIES = 100

#CITY_NAMES_PATH = 'cities.txt'
#CITY_NAMES_TYPE = 'txt'

CITY_NAMES_PATH = 'worldcities.csv'
CITY_NAMES_TYPE = 'csv'

USER_AGENT = 'Travel Agent (kevin.scott.maguire@gmail.com)'
CHROMA_COLLECTION_NAME = 'travel_agent'


class WikipediaPageDoesNotExistException(Exception):

    def __init__(self, page_name: str):            
        message = f"Wikipedia page with name '{page_name}' does not exist."
        super().__init__(message)


def get_page_by_name(page_name: str) -> wikipediaapi.WikipediaPage:
    """
    Get Wikipedia page by page name

    :raises WikipediaPageDoesNotExistException: If page does not exist.
    """
    wiki = wikipediaapi.Wikipedia(USER_AGENT, 'en')
    page = wiki.page(page_name)

    if not page.exists():
        raise WikipediaPageDoesNotExistException(page_name)

    return page


def get_page_with_full_text(page_name: str) -> wikipediaapi.WikipediaPage:
    """Get Wikipedia page by page name with full text"""
    wiki = wikipediaapi.Wikipedia(
        user_agent=USER_AGENT,
        language='en',
        extract_format=wikipediaapi.ExtractFormat.WIKI
        )
    page = wiki.page(page_name)

    if not page.exists():
        raise WikipediaPageDoesNotExistException(page_name)

    return page


def main():

    if CITY_NAMES_TYPE == 'txt':
        with open(CITY_NAMES_PATH) as cities_file:
            city_names = cities_file.read().split('\n')
    else:
        city_names = pd.read_csv(CITY_NAMES_PATH)['city_ascii'].tolist()[0:N_CITIES]

    chunker = TextChunker()

    document_dict = {}
    for i, city in enumerate(city_names):
        if i % 10 == 0:
            logger.info(f'Getting wikipedia page for city {i}/{N_CITIES}')
        try:
            city_page = get_page_with_full_text(city)
        except WikipediaPageDoesNotExistException as err:
            logger.warning(str(err))
            continue

        for chunk in chunker.chunk_text(city_page.text):
            id = str(hash(chunk))
            document_dict[id] = {'text': chunk, 'metadata': {'city_name': city, 'url': city_page.fullurl}}

    logger.info(f'Writing {len(document_dict)} documents to chromadb.')
    src.chromadb_helpers.write_documents(CHROMA_COLLECTION_NAME, document_dict)


if __name__ == '__main__':
    main()
