"""
The user gives a prompt of what kind of place they would like to go, and the agent responds with a Wikipedia link to 
a recommended city.

"""
import src.chromadb_helpers


CHROMA_COLLECTION_NAME = 'travel_agent'

INITIAL_PROMPT = '''
    Hi. I'm an AI travel agent. 
                
    Describe the kind of holiday you want and I will suggest a city.
                
    >>>'''

FINAL_PROMPT = '''
    You should vist {city_name}.

    You can find more information about {city_name} at this link: {link}

    {chunk_text}

    The distance measure was {distance}.
    '''


def main():
    
    response = input(INITIAL_PROMPT)

    try:
        _, metadata, chunk_text, distance = src.chromadb_helpers.query(CHROMA_COLLECTION_NAME, response)
    except src.chromadb_helpers.NoMatchingDocumentFoundException:
        print("Sorry, I don't have a city recommendation for you.")
        return

    print(FINAL_PROMPT.format(city_name=metadata['city_name'], link=metadata['url'], chunk_text=chunk_text, 
                              distance=distance))


if __name__ == '__main__':
    main()