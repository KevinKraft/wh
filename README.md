
# Introduction

The purpose of this task is to create a user support chat bot that can help with technical problems by pointing the user to support documents.

Due to lack of suitable data, I went in a slightly different direction. The agent created here is a virtual travel agent. From a user prompt, 
the agent will suggest a travel destination city, link the user to the wikipedia page for that city, and quote some relevant text from the wikipedia page. I hope this change is acceptable.

I have focused on the data enginnering role tasks:
* Design and implement a data pipeline persist text-based documents into a vector Database.
* Implement a chunking strategy to break the content into chunks.

The agent is backed by a vector database using ChromaDB running in a docker container. The vector database contains chunked text from the wikipedia pages of lots of cities.

By querying the database with the user prompt, the closest maching text chunk is returned. The matching chunk has associated metadata containing the city name and the Wikipedia URL.

# Conclusion

Overall the agent kind of works and is pretty fun to use.

I suspect ChromaDB is doing a lot of heavy lifting that the company might have wanted me to do manually, such as implementing a distance measure. I hope using ChromaDB is acceptable.

CODE QUALITY:
The code implemented is very simple, so I don't believe it displays my experiance with python very well. It is sufficient for this task.

TESTING: 
I haven't written unit tests due to time constraints.

DATA ENGINEERING DOCUMENTATION:
The data pipeline is very simple. A static CSV file containing city names is provided in the repo. This was downloaded from:
https://simplemaps.com/data/world-cities

Pandas is used to load this CSV and a list of city names is extracted.

A Wikipedia API is used to search a wiki page for each city.

Langchain text splitter is used to chunk the data.

Each chunk of data is stored in ChromaDB. The ID of each record is the hash of the text chunk to ensure uniqueness. Metadata stored alongside each record includes the city name and URL. The city name and URL also come from the Wikipedia API.

# Issues

1) Getting the wikipedia page data using the API is slow. For this reason the code is restricted to 100 cities. To improve this I could have used asyncio and queried the Wikipedia API from many running coroutines. In this way, we can 'actively wait' for the API to return results. This was not done due to time constraints.
2) There's no unit tests, due to time constraints.
3) I had difficulty setting up ChromaDB, and I suspect anyone using this repo will also have difficulty. To fix this the client code could be run inside the docker container, which already has the correct version of sqlite. This hasn't been done, and it is still necessary to install sqlite locally to get the client running (because its outside the docker container).

# Further work

If I was continuing work on this project, apart from fixing the issues above, I would:
1) Integrate the agent with chatGPT. The user prompt, and selected chunk of text can be supplied to chatGPT, and then the user can have a converstion about the text with the chatGPT agent. A big issue with this would be chatGPTs limited memory. It is too small to hold an entire wikipedia page. 
2) Add more cities.

# Setup

## Requirements

* python3.11
* Linux (I used WSL2 Unbuntu 20.04 on Windows 10)

## Python packages

See the Pipfile. You can use your preferred tool for installing and managing the dependencies. I used pipenv, which is already installed.

In the repo diretory
```
mkdir .venv
pipenv install
```

This will install the python packages in the Pipfile.


## Install sqlite >= 3.35

Chromadb requires sqlite3 >= 3.35. Chromadb runs in a docker container, which already has the correct vesion of sqlite. It's quite annoying as the client for chromadb used in this code doesn't even need sqlite, but the client wont import unless sqlite is installed.

Unfortunately this isn't packaged with my linux distribution so I had to install from source. 

This step is probably the most likely to go wrong for anyone wishing to use this repo.

Go to the repo directory and run these commands
(I've inclded this file in the repo if so you don't have to download it.)
```
wget https://sqlite.org/2024/sqlite-autoconf-3460000.tar.gz

tar -xvf sqlite-autoconf-3460000.tar.gz && cd sqlite-autoconf-3460000

./configure

make

sudo apt-get purge sqlite3

sudo make install

```

Run this next command and also add it to your bashrc file.
```
export PATH="/usr/local/bin:$PATH"  # also add in your .bashrc

```

Finally check the version
```
sqlite3 --version
```

# Running the code

## Start the database

```
make run-chromadb
```

## Load the city data into the database

The export command instructs python to read sqlite3 from the version we installed above, instead of whatever version is packaged with python.

Start your virtual environment, then run

```
export LD_LIBRARY_PATH="/usr/local/lib"
python load_wiki_data.py
```

This can take a few minutes to run.

Sample output:
```
INFO:__main__:Getting wikipedia page for city 0/100
INFO:__main__:Getting wikipedia page for city 10/100
INFO:__main__:Getting wikipedia page for city 20/100
INFO:__main__:Getting wikipedia page for city 30/100
INFO:__main__:Getting wikipedia page for city 40/100
INFO:__main__:Getting wikipedia page for city 50/100
INFO:__main__:Getting wikipedia page for city 60/100
INFO:__main__:Getting wikipedia page for city 70/100
INFO:__main__:Getting wikipedia page for city 80/100
INFO:__main__:Getting wikipedia page for city 90/100
WARNING:__main__:Wikipedia page with name 'Nangandao' does not exist.
INFO:__main__:Writing 3135 documents to chromadb.
```

## Run the AI travel agent

```
export LD_LIBRARY_PATH="/usr/local/lib"
python travel_agent.py
```

Sample output:
```

    Hi. I'm an AI travel agent. 
                
    Describe the kind of holiday you want and I will suggest a city.
                
    >>>I want to go somewhere with lots of nice beaches and restaurants

    You should vist Miami.

    You can find more information about Miami at this link: https://en.wikipedia.org/wiki/Miami

    Beaches and parks
The City of Miami has various lands operated by the National Park Service, the Florida Division of Recreation and Parks, and the City of Miami Department of Parks and Recreation.
Miami's tropical weather allows for year-round outdoor activities. The city has numerous marinas, rivers, bays, canals, and the Atlantic Ocean, which make boating, canoeing, sailing, and fishing popular outdoor activities. Biscayne Bay has numerous coral reefs that make snorkeling and scuba diving popular. There are over 80 parks and gardens in the city. The largest and most popular parks are Bayfront Park and Museum Park (located in the heart of Downtown and the location of the Miami-Dade Arena and Bayside Marketplace), Tropical Park, Peacock Park, Virginia Key, and Watson Island.
Other popular cultural destinations in or near Miami include Zoo Miami, Jungle Island, the Miami Seaquarium, Monkey Jungle, Coral Castle, Charles Deering Estate, Fairchild Tropical Botanic Garden, and Key Biscayne.
In its 2020 ParkScore ranking, The Trust for Public Land reported that the park system in the City of Miami was the 64th best park system among the 100 most populous U.S. cities, down slightly from 48th place in the 2017 ranking. The City of Miami was analyzed to have a median park size of 2.6 acres, park land as percent of city area of 6.5%, 87% of residents living within a 10-minute walk of a park, $48.39 spending per capita of park services, and 1.3 playgrounds per 10,000 residents.

    The distance measure was 0.9862641096115112.
```
