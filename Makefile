ROOT_DIR:=$(realpath $(shell dirname $(firstword $(MAKEFILE_LIST))))

delete-all-chroma-collections:
	# Requires restarting chroma afterwards
	rm -rf $(ROOT_DIR)/tmp/data/*


run-chromadb:
	mkdir -p ${PWD}/tmp/data
	docker run -p 8000:8000 -v ${PWD}/tmp/data:/chroma/chroma chromadb/chroma

