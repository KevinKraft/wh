"""
File for various functions to read, write and query chromadb.
"""
import chromadb
import typing

HOST = 'localhost'
PORT = 8000


class NoMatchingDocumentFoundException(Exception):
    """Exception for no document found in database."""


def get_client(host: str = HOST, port: int = PORT) -> chromadb.HttpClient:
    """Get chromadb client."""
    return chromadb.HttpClient(host=host, port=port)


def write_documents(collection_name: str, 
                    document_dict: dict[str, dict[str, dict[str, typing.Any]]]) -> None:
    """
    Write given dict value of document strings to chromadb collection with key ids
    
    :param collection_name: Name of collection to write to.
    :param document_dict: Dictionary with document IDs as the key. The value is a dict of two items: 'text' and 
        'metadata'. Text is the string text data. Metadata is a dict of metadata keys and values.
    """
    collection = get_client().get_or_create_collection(name=collection_name)

    text_data = [val['text'] for val in document_dict.values()]
    metadata = [val['metadata'] for val in document_dict.values()]

    collection.add(documents=text_data, ids=list(document_dict.keys()), metadatas=metadata)


def query(collection_name: str, search_text: str) -> typing.Union[str, dict[str, typing.Any]]:
    """
    Query the database and recommend a document

    :param collection_name: Name of collection to read from.
    :param search_text: Search prompt to use against the database.
    """
    collection = get_client().get_or_create_collection(name=collection_name)
    results = collection.query(query_texts=[search_text], n_results=1)

    if not results['ids'][0]:
        raise NoMatchingDocumentFoundException

    return (results['ids'][0][0], results['metadatas'][0][0], results['documents'][0][0], 
            results['distances'][0][0])


