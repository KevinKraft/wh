"""
Functions for chunking textual data using langchain.
"""

from langchain_text_splitters import RecursiveCharacterTextSplitter


class TextChunker:

    def __init__(self) -> None:
        """Contructor."""
        self._text_splitter = RecursiveCharacterTextSplitter(
            chunk_size = 2048,
            chunk_overlap = 20,
            length_function = len,
            )

    def chunk_text(self, text: str) -> list[str]:
        """Chunk the given text using langchain."""
        return [doc.page_content for doc in self._text_splitter.create_documents([text])]
